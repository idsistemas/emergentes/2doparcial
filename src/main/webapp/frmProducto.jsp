<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DAO - frmProducto</title>
        <link rel="stylesheet" href="assets/bootstrap.css"/>
    </head>
    <body>
        <div class="container my-3">
            <div class="card py-3 px-5 mb-5">
                <div class="px-5">
                    <p>SEGUNDO PARCIAL TEM-742</p>
                    <p><b>Nombre: </b>Gary Limbert Apaza Mamani</p>
                    <p><b>Carnet: </b>10077835 LP</p>
                </div>
            </div>
            <h1>
                <c:if test="${producto.id == 0}">
                    Nuevo producto
                </c:if>
                <c:if test="${producto.id != 0}">
                    Editar producto
                </c:if>
            </h1>
            <form action="Inicio" method="POST">
                <input type="hidden" name="id" value="${producto.id}"/>
                <div class="form-group">
                    <label class="form-label" for="descripcion">.:: Descripcion ::.</label>
                    <input class="form-control" type="text" name="descripcion" id="descripcion" value="${producto.descripcion}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="cantidad">.:: Cantidad ::.</label>
                    <input class="form-control" type="number" min="0" name="cantidad" id="cantidad" value="${producto.cantidad}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="precio">.:: Precio ::.</label>
                    <input class="form-control" type="number" min="0" name="precio" id="precio" value="${producto.precio}" step="any">
                </div>
                <div class="form-group">
                    <label class="form-label" for="categoria">.:: Categoria ::.</label>
                    <input class="form-control" type="text" name="categoria" id="categoria" value="${producto.categoria}">
                </div>
                <c:if test="${producto.id == 0}">
                    <input type="submit" value="Agregar producto" class="btn btn-success mt-3">
                </c:if>
                <c:if test="${producto.id != 0}">
                    <input type="submit" value="Editar producto" class="btn btn-warning mt-3">
                </c:if>
                <a class="btn btn-primary mt-3" href="Inicio">Volver</a>
            </form>
        </div>
    </body>
</html>
