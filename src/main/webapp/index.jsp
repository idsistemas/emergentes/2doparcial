<%@page import="java.util.List"%>
<%@page import="com.emergentes.models.Producto"%>
<%
    List<Producto> productos = (List<Producto>) request.getAttribute("productos");
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>2do Parcial</title>
        <link rel="stylesheet" href="assets/bootstrap.css"/>
    </head>
    <body>
        <div class="container my-3">
            <div class="card py-3 px-5 mb-5">
                <div class="px-5">
                    <p>SEGUNDO PARCIAL TEM-742</p>
                    <p><b>Nombre: </b>Gary Limbert Apaza Mamani</p>
                    <p><b>Carnet: </b>10077835 LP</p>
                </div>
            </div>
            <h1 class="text-center">GESTION DE PRODUCTOS</h1>
            <a href="Inicio?op=add" class="btn btn-primary">Nuevo producto</a>
            <table class="table table-bordered table-hover m-3">
                <tr class="bg-dark text-white">
                    <th>Id</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Categoria</th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach var="item" items="${productos}">
                    <tr>
                        <td>${item.id}</td>
                        <td>${item.descripcion}</td>
                        <td>${item.cantidad}</td>
                        <td>${item.precio}</td>
                        <td>${item.categoria}</td>
                        <td><a href="Inicio?op=edit&id=${item.id}" class="btn btn-sm btn-warning">Editar</a></td>
                        <td><button class="btn btn-sm btn-danger" onclick="confirmDelete(${item.id})">Eliminar</button></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <script src="assets/sweetalert2.js"></script>
        <script>
                            function confirmDelete(id) {
                                Swal.fire({
                                    title: 'Eliminar registro',
                                    text: "¿Esta seguro de eliminar el registro?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Si, eliminar',
                                    cancelButtonText: 'Cancelar'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        Swal.fire({
                                            title: 'Eliminado',
                                            text: 'El registro a sido eliminado correctamente.',
                                            icon: 'success',
                                            showConfirmButton: true,
                                            confirmButtonText: 'Aceptar'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                location.href = "Inicio?op=delete&id=" + id;
                                            }
                                        });
                                    }
                                });
                            }
        </script>
    </body>
</html>
