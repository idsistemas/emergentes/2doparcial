CREATE DATABASE bd_almacen CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE productos (
  id int(11) AUTO_INCREMENT PRIMARY KEY,
  descripcion varchar(100),
  cantidad int(11),
  precio float(11,2),
  categoria varchar(50) 
);

INSERT INTO productos (descripcion, cantidad, precio, categoria) VALUES
('Fanta', 200, 10.00, 'Bebidas'),
('Sprite', 200, 10.00, 'Bebidas');
