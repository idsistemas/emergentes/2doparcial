
## 2do Parcial - Tecnologias Emergentes II

#### Universitario: Gary Limbert Apaza Mamani
#### CI: 10077835 LP
#### Semestre: 7mo
#


### Dependencias Maven

| Nombre dependencia             | Version  |
| :----------------------------- | :------- |
| `JavaEE web api`               | `8.0.1`  |
| `mysql connector java wwe api` | `8.0.33` |
| `jstl`                         | `1.2`    |
